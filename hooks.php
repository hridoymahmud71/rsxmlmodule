<?php
/**
 * WHMCS SDK Sample Addon Module Hooks File
 *
 * Hooks allow you to tie into events that occur within the WHMCS application.
 *
 * This allows you to execute your own code in addition to, or sometimes even
 * instead of that which WHMCS executes by default.
 *
 * @see https://developers.whmcs.com/hooks/
 *
 * @copyright Copyright (c) WHMCS Limited 2017
 * @license http://www.whmcs.com/license/ WHMCS Eula
 */

// Require any libraries needed for the module to function.
// require_once __DIR__ . '/path/to/library/loader.php';
//
// Also, perform any initialization required by the service's library.

//-------------------------------------------------------------
$module_name = "rsxmlmodule";
$module_title = "RSXML MODULE";

define("MODULE_NAME", $module_name);
define("MODULE_TITLE", $module_title);
define("MODULE_DIR", $_SERVER["DOCUMENT_ROOT"] . "/modules/addons/{$module_name}");


//-------------------------------------------------------------

/**
 * Register a hook with WHMCS.
 *
 * This sample demonstrates triggering a service call when a change is made to
 * a client profile within WHMCS.
 *
 * For more information, please refer to https://developers.whmcs.com/hooks/
 *
 * add_hook(string $hookPointName, int $priority, string|array|Closure $function)
 */
add_hook('ClientEdit', 1, function (array $params) {
    try {
        // Call the service's function, using the values provided by WHMCS in
        // `$params`.
    } catch (Exception $e) {
        // Consider logging or reporting the error.
    }
});

add_hook('AdminHomeWidgets', 1, function () {
    return new RSXML_Widget();
});

/**
 * Hello World Widget.
 */
class RSXML_Widget extends \WHMCS\Module\AbstractWidget
{
    protected $title = MODULE_TITLE . ' Widget';
    protected $description = '';
    protected $weight = 150;
    protected $columns = 1;
    protected $cache = false;
    protected $cacheExpiry = 120;
    protected $requiredPermission = '';

    public function getData()
    {
        return array();
    }

    public function generateOutput($data)
    {
        $module_name = MODULE_NAME;
        $module_title = MODULE_TITLE;

        return <<<EOF
        <div class="widget-content-padded">
           Go to <a href="addonmodules.php?module=$module_name">$module_title</a>
        </div>
EOF;
    }
}

add_hook('AdminAreaHeaderOutput', 1, function($vars) {
    $return = '';
    if (array_key_exists('project_management', $vars['addon_modules'])) {
        $return = '<b>This is a custom output on the header when Project Management is enabled</b>';
    }
    return $return;
});

