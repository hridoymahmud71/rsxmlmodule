<?php

namespace WHMCS\Module\Addon\RsxmlModule\Client;


/**
 * Sample Client Area Controller
 */
class Controller
{

    public function __construct()
    {
        $this->trigger_error_reporting(1);
    }

    function trigger_error_reporting($trigger)
    {

        if ($trigger) {
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
        }

    }

    /**
     * Index action.
     *
     * @param array $vars Module configuration parameters
     *
     * @return array
     */

/*    public function index($vars)
    {
        // Get common module parameters
        $modulelink = $vars['modulelink']; // eg. rsxmlmodule.php?module=rsxmlmodule
        $version = $vars['version']; // eg. 1.0
        $LANG = $vars['_lang']; // an array of the currently loaded language variables

        // Get module configuration parameters
        $configTextField = $vars['Text Field Name'];
        $configPasswordField = $vars['Password Field Name'];
        $configCheckboxField = $vars['Checkbox Field Name'];
        $configDropdownField = $vars['Dropdown Field Name'];
        $configRadioField = $vars['Radio Field Name'];
        $configTextareaField = $vars['Textarea Field Name'];


        return array(
            'pagetitle' => 'RS Module',
            'breadcrumb' => array(
                'index.php?m=rsxmlmodule' => 'RSXML Module',
            ),
            'templatefile' => 'publicpage',
            'requirelogin' => false, // Set true to restrict access to authenticated client users
            'forcessl' => false, // Deprecated as of Version 7.0. Requests will always use SSL if available.
            'vars' => array(
                'modulelink' => $modulelink,
                'configTextField' => $configTextField,
                'customVariable' => 'your own content goes here',
            ),
        );
    }*/

    /**
     * Secret action.
     *
     * @param array $vars Module configuration parameters
     *
     * @return array
     */

    /*public function secret($vars)
    {
        // Get common module parameters
        $modulelink = $vars['modulelink']; // eg. rsxmlmodule.php?module=rsxmlmodule
        $version = $vars['version']; // eg. 1.0
        $LANG = $vars['_lang']; // an array of the currently loaded language variables

        // Get module configuration parameters
        $configTextField = $vars['Text Field Name'];
        $configPasswordField = $vars['Password Field Name'];
        $configCheckboxField = $vars['Checkbox Field Name'];
        $configDropdownField = $vars['Dropdown Field Name'];
        $configRadioField = $vars['Radio Field Name'];
        $configTextareaField = $vars['Textarea Field Name'];

        return array(
            'pagetitle' => 'RS Module',
            'breadcrumb' => array(
                'index.php?m=rsxmlmodule' => 'RSXML Module',
                'index.php?m=rsxmlmodule&action=secret' => 'Secret Page',
            ),
            'templatefile' => 'secretpage',
            'requirelogin' => true, // Set true to restrict access to authenticated client users
            'forcessl' => false, // Deprecated as of Version 7.0. Requests will always use SSL if available.
            'vars' => array(
                'modulelink' => $modulelink,
                'configTextField' => $configTextField,
                'customVariable' => 'your own content goes here',
            ),
        );
    }*/


}
